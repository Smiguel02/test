package com.example.demo;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCombination;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;


/**
 * Basically this has hierarchy structure
 * We follow this: Stage->Scene->Scene Graph->Root Nodes
 */

public class HelloApplication extends Application {
    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("hello-view.fxml"));
        Group root = new Group();
        Scene scene = new Scene(root, Color.BLUEVIOLET);  //fxmlLoader.load()
        stage.setTitle("Hello my friend, this is Estoril 1942!");   //Title da window
        Image icon = new Image("imagem_epica.png");
        stage.getIcons().add(icon);
        stage.setScene(scene);
        stage.show();       //to show the stage
        /**
         * All of the code below is for the root, is a way of doing things manually
         * Leaving it here in case I need this later
         */

        Text text = new Text("HEy we aliveeeeashdahsdhsaden");
        text.setX(50);
        text.setY(50);
        text.setFont(Font.font("Arial", 50));

        Line line = new Line();
        line.setStartX(100);
        line.setStartY(50);
        line.setEndX(450);
        line.setEndY(50);
        line.setFill(Color.DARKGRAY);

        Image image = new Image("exodia_head.jpg");
        ImageView view_image = new ImageView(image);
        view_image.setX(200);
        view_image.setY(200);
        view_image.setFitWidth(100);
        view_image.setFitHeight(200);
        //view_image.setOpacity(0.2);

        //stage.setWidth(400);
        //stage.setHeight(600);
        stage.setResizable(false);
        stage.setFullScreen(true);
        stage.setFullScreenExitHint("You can't escape unless you press q");
        stage.setFullScreenExitKeyCombination(KeyCombination.valueOf("q"));
        root.getChildren().add(text);
        root.getChildren().add(line);
        root.getChildren().add(view_image);



    }

    public static void main(String[] args) {
        launch();
    }   //Launches application
}